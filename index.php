<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset=UTF-8>
        <title>Test PHP</title>
        <link href="file.css" rel="style">
    </head>
    <body>
        <?php
            function callAPI($url, $ch): array
            {
                $optArray = array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true
                );
                curl_setopt_array($ch, $optArray);
                $result = curl_exec($ch);
                if ($result == "Not Found") {
                    //echo "<p>error : url : $url</p>";
                    return array();
                }
                return json_decode($result, true);
            }


            function printUsedType($ch)
            {
                $delimitateur = " ";
                foreach ($_GET as $value){
                    if ($value == 'an' || $value == 'fr') {
                        continue;
                    }

                    if ($_GET["lang"] == "fr") {
                        $tabBis = callAPI("https://pokeapi.co/api/v2/type/{$value}", $ch);
                        $value = $tabBis["names"][3]["name"];
                    }
                    echo $delimitateur . $value;
                    $delimitateur = ", ";
                }
            }


            //test for select a language
            if (empty($_GET)) {
                $_GET["lang"] = "fr";
            }

            $textQuest = "choose your types";
            $textChange = "change";
            $textFr = "French";
            $textAn = "English";
            $textTete = "Change in";
            if ($_GET["lang"] == "fr") {
                $textQuest = "choisisser vos type";
                $textChange = "changer";
                $textFr = "Français";
                $textAn = "anglais";
                $textTete = "changer en ";
            }

            echo "<form action=\"#\" method=\"get\">";

            foreach ($_GET as $key => $value) {
                if ($key != 'lang') {
                    echo "<input type= \"hidden\" name=\"" . $key . "\" value=\"" . $value . "\">";
                }
            }

            echo "
                <div class=\"button\">
                <label for=\"lang\">$textTete </label>
                <select name=\"lang\" id=\"lang-select\">
                    <option value=\"fr\">$textFr</option>
                    <option value=\"an\">$textAn</option>
                </select>
                <button type=\"submit\">$textChange</button>
                </div>
            </form>
            ";

            echo "<p>$textQuest</p>";

            echo "<form action=\"#\" method=\"get\">";

            $ch = curl_init();

            $tab = callAPI("https://pokeapi.co/api/v2/type", $ch);

            // affiche les box de sélections

            echo "<input type= \"hidden\" name=\"lang\" value=\"" . $_GET['lang'] . "\">";
            foreach ($tab["results"] as $value) {
                $info = $value['name'];
                if ($_GET["lang"] == "fr") {
                    $tabBis = callAPI("https://pokeapi.co/api/v2/type/{$value['name']}", $ch);
                    $value = $tabBis["names"][3];
                }
                // affiche les box de sélections
                echo "<p>" ;
                echo "
                <input type= \"checkbox\" name=\"{$info}\"
                value=\"{$info}\">
                {$value["name"]}
                <br>
                " ;
                echo "</p>" ;
            }

            $textValid = "research pokemons with this types";
            $textReset = "reset";
            if ($_GET["lang"] == "fr") {
                $textValid = "rechercher les pokemons avec ces type";
                $textReset = "annuler";
            }

            echo "
            <div class=\"button\">
                <button type=\"submit\">
                    $textValid
                </button>
            </div>

            <div class=\"button\">
                <button type=\"reset\">$textReset</button>
            </div>
            ";

            echo "</form>";

            if(count($_GET) > 1){
                $sauv = array();
                foreach ($_GET as $value) {
                    if ($value == 'an' || $value == 'fr') {
                        continue;
                    }

                    $poke = array();

                    $tab = callAPI("https://pokeapi.co/api/v2/type/$value", $ch);

                    // affiche les box de sélections
                    foreach ($tab["pokemon"] as $key => $value) {
                        $poke[] = $value["pokemon"]["name"];
                    }

                    array_push($sauv, $poke);
                }
                //var_dump($sauv[0]);
                foreach ($sauv as $take) {
                    $sauv[0] = array_intersect($sauv[0], $take);
                }
                //var_dump($sauv[0]);
                $delimitateur = "";

                if (empty($sauv[0])) {
                    if ($_GET["lang"] == "fr") {
                        echo "<p> aucun pokemon avec les types: ";
                    } else {
                        echo "<p> not find pokemon with types: ";
                    }

                    printUsedType($ch);

                    echo "</p>";
                } else {
                    if ($_GET["lang"] == "fr") {
                        echo "<p> liste des pokemons avec les types: ";
                    } else {
                        echo "<p> list pokemon with types: ";
                    }

                    printUsedType($ch);

                    echo " : </p>";
                    echo "<ul>";

                    $count = 1;
                    foreach ($sauv[0] as $poke) {
                        $tabBis = callAPI("https://pokeapi.co/api/v2/pokemon/{$poke}", $ch);
                        echo "
                        <li>
                            <div class=poke>
                        ";

                        if ($_GET["lang"] == "fr") {
                            $tabSpe = callAPI("https://pokeapi.co/api/v2/pokemon-species/{$poke}", $ch);

                            if (!empty($tabSpe['names'][4]['name'])) {
                                echo "    {$tabSpe['names'][4]['name']} ";
                            } else {
                                echo "$poke";
                            }
                        } else {
                            echo "$poke";
                        }
                        echo "<p>($count)</p>";
                        $count ++ ;
                        echo "<img src=\"{$tabBis['sprites']['front_default']}\">";
                        echo "<ul>";

                        foreach ($tabBis['stats'] as $val) {
                            echo "<li>{$val['stat']['name']} : {$val['base_stat']}</li>";
                        }

                        echo "</ul>";

                        echo "<p>Version : ";

                        if (empty($tabBis['game_indices'])) {
                            if ($_GET["lang"] == "fr") {
                                echo " aucune";
                            } else {
                                echo " none";
                            }
                        }

                        foreach ($tabBis['game_indices'] as $val) {
                            echo "{$val['version']['name']} ";
                        }

                        echo "<p>";

                        echo "</div></li>";
                    }

                    echo "</ul>";
                }
            }
            curl_close($ch);
        ?>
    </body>
</html>
